<?php

 function Messages() {

 	if (session_status() !== 0 && session_status() !== 'PHP_SESSION_DISABLED') {

		if (isset($_SESSION['message'])) {
	 		echo $_SESSION['message'];
	 		unset($_SESSION['message']);
	 	}
	} else {
		echo("Sessions are disabled.");
	}

 }
