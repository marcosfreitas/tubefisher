<?php

try {


	//ob_start();// if not, some servers will show this php warning: header is already set in line 46...
	//include_once('class/objecthelper.class.php');
	//include_once('class/superclass.class.php');
	include_once('classes/config.interface.php');
	include_once('classes/curl.class.php');
	include_once('classes/get-video.class.php');

	if(!isset($_GET['mime']) || empty($_GET['mime']) || !isset($_GET['token']) || empty($_GET['token'])) {
		throw new Exception("Invalid request, try again.", 1);
	}


	// Set operation params
	$mime = filter_var($_GET['mime']);
	$ext  = str_replace(array('/', 'x-'), '', strstr($mime, '/'));
	$url  = base64_decode(filter_var($_GET['token']));
	$name = urldecode($_GET['title']). '.' .$ext;

// Fetch and serve
if (!empty($url)) {

	$curl = new Curl();
	$file_size = $curl->get_size($url);

	if ($file_size !== false) {
		// Generate the server headers
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) {
			header('Content-Type: "' . $mime . '"');
			header('Content-Disposition: attachment; filename="' . $name . '"');
			header('Expires: 0');
			header('Content-Length: '.$file_size);
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
		} else {
			header('Content-Type: "' . $mime . '"');
			header('Content-Disposition: attachment; filename="' . $name . '"');
			header("Content-Transfer-Encoding: binary");
			header('Expires: 0');
			header('Content-Length: '.$file_size);
			header('Pragma: no-cache');
		}

		readfile($url);

		// download successfull
		$_SESSION['message'] = "That video is downloading... thank you.";
		exit;
	}
}

// throw an exception
throw new Exception("The download couldn't be processed, please try again.", 1);

} catch (Exception $e) {
	$_SESSION['message'] = $e->getMessage();
	header('Location: /');
	exit;
}
