<?php include_once("config.php"); ?>

<!DOCTYPE html>
<!--[if gte IE 9]><html dir="ltr" lang="pt-BR" class="ie9 ie"><![endif]-->
<!--[if IE 8]><html dir="ltr" lang="pt-BR" class="ie8 ie"><![endif]-->
<!--[if lte IE 7]><html dir="ltr" lang="pt-BR" class="ie7 ie"><![endif]-->
<!--[if !IE]><!--><html dir="ltr" lang="pt-BR"><!--<![endif]-->
<head>

	<!-- seo -->
	<?php include("includes/gt-seo.php"); ?>
	<?php include("includes/gt-head.php"); ?>

</head>

</head>
<body class="gt-home">

	<div class="ui centered grid container">
		<div class="row">
			<div class="column">

				<h2 class="ui huge header">Tube Fisher <small class="ui left pointing mini label">1.1 beta</small></h2>

				<!--<div class="ui message"><small>adversement</small></div>-->

				<?php
					$is_attached = '';

					if (!empty($_SESSION['message'])):
						$is_attached = 'attached';
				?>
					<div class="ui icon error large message">
						<p><?php Messages(); ?></p>
					</div>
				<?php
					endif;

					//if (empty($is_attached)) :
				?>
				<div class="ui attached message" style="text-align: left;">
					<div class="header">Paste the video URL or ID.</div>
					<p>https://youtube.com/watch?v=<strong>7jE-ODROpyY</strong></p>
				</div>

				<?php
					//endif;
				?>

				<div class="ui attached stacked segment">
					<form id="form-home" class="ui large form" action="get-video" method="get">
					    <div class="field">
					      <div class="ui left icon input">
					        <i class="linkify icon"></i>
					        <input type="text" name="video" placeholder="Video URl or ID">
					      </div>
					    </div>
					    <button type="submit" class="ui fluid large orange submit button"><i class="lightning icon"></i> Get this video!</button>
					    <div class="ui error message"></div>
					</form>
				</div>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
				<script src="assets/vendor/semantic-ui/semantic.min.js" type="text/javascript" charset="utf-8"></script>
				<script>
					$('#form-home')
					  .form({
					    fields: {
					      video : {
					      	rules : [{
						      	type : 'empty'
					      	}]
					      }
					    },
					  })
					;
				</script>

				<!--<div class="ui message">
				 <small>adversement</small>
				</div>-->

				<?php include("includes/gt-footer.php"); ?>

			</div>
		</div>
	</div>

</body>
</html>
