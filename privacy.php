<?php include_once("config.php"); ?>

Politicas de Privacidade

O BaixarTube.com.br pode utilizar cookies e/ou web beacons quando um usu�rio tem acesso �s p�ginas. Os cookies que podem ser utilizados e associam-se (se for o caso) unicamente com o navegador do usu�rio. Os cookies que s�o utilizados no BaixarTube.com.br podem ser instalados pelo mesmo, os quais s�o originados dos distintos servidores operados por este, ou a partir dos servidores de terceiros que prestam servi�os e instalam cookies e/ou web beacons (por exemplo, os cookies que s�o empregados para prover servi�os de publicidade ou certos conte�dos atrav�s dos quais o usu�rio visualiza a publicidade ou conte�dos em tempos pr� determinados). O usu�rio poder� pesquisar os cookies no disco r�gido de seu computador conforme instru��es do pr�prio navegador. O Usu�rio tem a possibilidade de configurar seu navegador para ser avisado, na tela do computador, sobre a recep��o dos cookies e para impedir a sua instala��o no disco r�gido. As informa��es pertinentes a esta configura��o est�o dispon�veis em instru��es e manuais do pr�prio navegador.

Aten��o: Esse site n�o pertence ao youtube, � um site independente com o intuito de ajudar os usu�rios.
