<?php

include_once("config.php");

	/**
	 * Get Tube
	 * based on https://github.com/jeckman/YouTube-Downloader
	 * Takes a VideoID and outputs a list of formats in which the video can be downloaded.
	 */

try {

	//ob_start();// if not, some servers will show this php warning: header is already set in line 46...
	//include_once('class/objecthelper.class.php');
	//include_once('class/superclass.class.php');
	include_once('classes/config.interface.php');
	include_once('classes/curl.class.php');
	include_once('classes/get-video.class.php');

	if(isset($_GET['video']) && !empty($_GET['video'])) {
		// instance of class
		$video = new GetVideo($_GET['video']);
	} else {
		throw new Exception("Put the video URL or ID into field.", 1);
	}

} catch (Exception $e) {
	$_SESSION['message'] = $e->getMessage();
	header('Location: /');
	exit;
}


?>
<!DOCTYPE html>
<!--[if gte IE 9]><html dir="ltr" lang="pt-BR" class="ie9 ie"><![endif]-->
<!--[if IE 8]><html dir="ltr" lang="pt-BR" class="ie8 ie"><![endif]-->
<!--[if lte IE 7]><html dir="ltr" lang="pt-BR" class="ie7 ie"><![endif]-->
<!--[if !IE]><!--><html dir="ltr" lang="pt-BR"><!--<![endif]-->

<meta name="theme-color" content="#00b5ad">
<head>

	<!-- seo -->
	<?php include("includes/gt-seo.php"); ?>
	<?php include("includes/gt-head.php"); ?>

</head>

</head>
<body class="gt-download">

	<div class="ui centered grid container">

		<div class="row">
			<div class="column">

				<h2 class="ui huge header">Tube Fisher <small class="ui left pointing mini label">1.1 beta</small></h2>

					<!--<div class="ui message">
					<small>adversement</small>
					</div>-->

				<?php echo $video->html_message; ?>
				<div class="ui attached segment">
				<!--<div class="ui icon teal message">
					<i class="notched circle loading icon"></i>
					<div class="content">
						<div class="header">Just one second.</div>
						<p>We're fetching that video for you.</p>
					</div>
				</div>-->

				<?php echo $video->html_response; ?>
				</div>

				<a href="/" class="ui teal bottom attached icon button"><i class="repeat icon"></i> Download another video</a>

				<!--<div class="ui message">
					<small>adversement</small>
				</div>-->

				<?php include("includes/gt-footer.php"); ?>

			</div>
		</div>

	</div>


</body>
</html>
