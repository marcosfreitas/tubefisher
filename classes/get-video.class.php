<?php


class GetVideo extends Curl {

	public $messages = "";
	// requested video
	public $video = "";
	public $video_id = "";
	public $video_info = "";
	public $video_formats_array = [];
	public $video_thumb = "";
	public $video_title = "";

	public $html_response = "";
	public $html_message = "";

	public function __construct($video) {

	        $this->video = $video;
			$this->video_id = self::parse_url($video);

			if ($this->video_id === false) {

				$this->video_id = self::parse_video_id($video);
				if ($this->video_id === false) {
					throw new Exception("I'm sorry, We don't support your URL. Can you send her for us? (marcosfreitas@c4network.com.br)");
				}

			}

			// init curl constructor
			parent::__construct();

			// First get the video info page for this video id
			// - video details
			self::video_info();

			// close curl
			parent::close_curl();

			// create an array of available download formats
			// deppends of video_info() pass successfull
			if (count($this->video_formats_array) > 0) {
				// renew curl headers init that again
				parent::__construct();

				$this->html_response = self::available_downloads();

				// close curl
				parent::close_curl();
			}

    }


	/**
	 *  Check if input string is a valid YouTube URL
 	 *  and try to extract the YouTube Video ID from it.
	 *  @access  private
	 *  @return  mixed    Returns YouTube Video ID, or (boolean) FALSE.
	 */

	 private function parse_url($url) {

        $pattern = '/^(?:https?:)?'; # Optional URL scheme. Either http or https.
        $pattern .= '(?:\/\/)?'; # maybe have only //
        $pattern .= '(?:www\.)?'; #  Optional www subdomain.
        #  Group host alternatives:
	        $pattern .= '(?:youtu|youtube|m\.youtube)'; # variations of youtube.com
	        $pattern .= '(?:\.[a-z]{2,3})'; # second-level domain (SLD)... .com[...], .co[...], etc.
	        $pattern .= '(?:\.[a-z]{2})?'; # top-level domain or TLD .br, .es, etc.
	        $pattern .= '\/?'; # separator bar
	        $pattern .= '(?:'; # Group path alternatives:
	        	$pattern .= 'embed\/|\/v\/|\/watch\?v=|\/watch\?.+&v=';
	        $pattern .= ')?'; # End path alternatives.
        #  End host alternatives.
        $pattern .= '([\w-]{11})'; # 11 characters (Length of Youtube video ids).
        $pattern .= '(?:.+)?/'; # Optional other ending URL parameters.

        preg_match($pattern, $url, $matches);

        return (isset($matches[1])) ? $matches[1] : false;
    }

    private function parse_video_id($id) {
    	$pattern = '/^([\w-]{11})/'; # only video id
	        preg_match($pattern, $id, $matches);
	        return (isset($matches[1])) ? $matches[1] : false;
    }

    /**
	 *  Get video's info page for the current video
	 *  @access  private
	 *  @return  null If error happens trigger an exception
	 */
    private function video_info() {

		$this->video_info = 'http://www.youtube.com/get_video_info?&video_id='. $this->video_id .'&asv=3&el=detailpage&hl=en_US';

		$this->video_info = self::get_file($this->video_info);


		// details of that video
		$thumbnail_url = $title = $url_encoded_fmt_stream_map = $type = $url = "";

		// making variables from the video info string
		// - variables have same name of that string's parameters
		parse_str($this->video_info);

		// populate properties
		$this->video_thumb = $thumbnail_url;
		$this->video_title = $title;

		if (isset($url_encoded_fmt_stream_map) && !empty($url_encoded_fmt_stream_map)) {

			// formats of that video
			$this->video_formats_array = explode(',', $url_encoded_fmt_stream_map);

		} else {
			throw new Exception("We couldn't find an encoded format stream. Check if the URL of that video or if he exists or maybe that video can not be downloaded.", 1);
		}
    }

    // signature decode
    private function sig_decode() {
    	///
    }
    /**
	 *  Prepare the links for download
	 *  @access  private
	 *  @return  null If error happens trigger an exception
	 */
    private function available_downloads() {

    	$video_formats = [];
    	$index = 0;
    	$ipbits = $ip = $itag = $sig = $quality = '';
		$expire = time();

    	foreach ($this->video_formats_array as $format) {
    		parse_str($format);

			$video_formats[$index]['itag'] = $itag;
			$video_formats[$index]['quality'] = $quality;
			$type = explode(';',$type);
			$video_formats[$index]['type'] = $type[0];
			$video_formats[$index]['url'] = urldecode($url) . '&signature=' . $sig;
			parse_str(urldecode($url));
			$video_formats[$index]['expires'] = date("G:i:s T", $expire);
			$video_formats[$index]['ipbits'] = $ipbits;
			$video_formats[$index]['ip'] = $ip;

			$index++;
    	}

    	#$messages .= '<p>These links will expire at '. $video_formats[0]['expires'] .'</p>';
		#$messages .= '<p>The server was at IP address '. $video_formats[0]['ip'] .' which is an '. $video_formats[0]['ipbits'] .' bit IP address. ';
		#$messages .= 'Note that when 8 bit IP addresses are used, the download links may fail.</p>';

		//var_dump($video_formats);

		// variables for the formats

		$this->html_message = '<div class="ui icon attached teal message">';
			$this->html_message .= '<i class="download icon"></i>';
			$this->html_message .= '<div class="content">';
				$this->html_message .= '<div class="header">Press one of the buttons bellow to download that video in different formats.</div>';
				$this->html_message .= '<p>These links will expire at '. $video_formats[0]['expires'] .'</p>';
			$this->html_message .= '</div>';
		$this->html_message .= '</div>';




		/// link download
		//$directlink = explode('.googlevideo.com/', $video_formats[$index]['url']);
		//$directlink = 'http://redirector.googlevideo.com/' . $directlink[1] . '';
		//  $messages .= '<a href="' . $directlink . '&title='.$cleanedtitle.'" class="mime">' .  . '</a> ';

		$html = "";


		// thumbnail
		$html .= '<div class="ui items">';
			$html .= '<div class="item">';
				$html .= '<div class="ui tiny image">';
					$html .= '<img src="'. $this->video_thumb .'">';
				$html .= '</div>';
				$html .= '<div class="middle aligned content">';
					$html .= '<span class="header">'. $this->video_title .'</span>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';

		foreach ($video_formats as $format) {

			#$html .= 'Download in <div class="ui label">' . $video_formats[$i]['type'] .'</div>';
			#$html .= '<a class="ui youtube button" href="download.php?mime=' . $format['type'] .'&title='. urlencode($this->video_title) .'&token='.base64_encode($format['url']) . '" class="dl">';
			#	$html .= '<i class="youtube play icon"></i> ' . $format['type'];
			#$html .= '</a>';
			#$video_format['quality'];

			$html .= '<div class="ui labeled button">';
				$html .= '<a href="download.php?mime=' . $format['type'] .'&title='. urlencode($this->video_title) .'&token='.base64_encode($format['url']) . '">';
					$html .= '<div class="ui mini youtube button">';
						$html .= '<i class="download icon"></i> ' . $format['type'];
					$html .= '</div>';
					$html .= '<div class="ui basic red left pointing label">';
					// getting via curl the size of the video type and convert the bytes
					$html .= self::human_bytes($format['url']);
					$html .= '</div>';
				$html .= '</a>';
			$html .= '</div>';
		}

		/*<div class="ui divided items">
			<div class="item">
				<div class="image">
					<img src="/images/wireframe/image.png">
				</div>
				<div class="content">
					<a class="header">12 Years a Slave</a>
					<div class="meta"><span class="cinema">Union Square 14</span></div>
					<div class="description"></div>
					<div class="extra">
						<div class="ui label">IMAX</div>
						<div class="ui label"><i class="globe icon"></i> Additional Languages</div>
					</div>
				</div>
			</div>
		</div>*/

		return $html;

    }


	/**
     *  Format file size in bytes into human-readable string.
     *  @access  public
     *  @param   string   $bytes   Filesize in bytes.
     *  @return  string   Returns human-readable formatted filesize.
     */
    public function human_bytes($url) {

        $bytes = parent::get_size($url);
        $video_size =  'oops.';

        switch ($bytes):
            case $bytes < 1024:
                $video_size = $bytes .' B'; break;
            case $bytes < 1048576:
                $video_size = round($bytes / 1024, 2) .' KiB'; break;
            case $bytes < 1073741824:
                $video_size = round($bytes / 1048576, 2) . ' MiB'; break;
            case $bytes < 1099511627776:
                $video_size = round($bytes / 1073741824, 2) . ' GiB'; break;
        endswitch;

        return $video_size;
    }


}
