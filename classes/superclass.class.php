<?php

class Superclass extends ObjectHelper {
	public function __get($name){

	    if (ObjectHelper::existsMethod($this,$name)){
	        return $this->$name();
	    }

	    return null;
	}

	public function __set($name, $value){

	    if (ObjectHelper::existsMethod($this,$name))
	        $this->$name($value);
	}
}
