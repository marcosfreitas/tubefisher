<?php


class Curl implements cnfg {

	//private $outgoing_ip = ""; // for diferent ips requests
	public $path = "";
	public $ch = null;
	public $curl = null;

	private $CURL_UA = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36";
	private $CURL_REFERER = "http://www.google.com/";


	public function __construct() {
		$this->ch = curl_init();
	}

	public function __desctruct() {
		self::close_curl();
	}

	public function close_curl() {
		curl_close($this->ch);
		$this->curl = null;
	}

	/*
	 * function to get via cUrl
	 * From lastRSS 0.9.1 by Vojtech Semecky, webmaster @ webdot . cz
	 * See      http://lastrss.webdot.cz/
	 */

	/*public function curl_get_file($url) {
		$ch = curl_init();
	    $timeout = 3;
	    curl_setopt( $ch , CURLOPT_URL , $url );
	    curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
	    curl_setopt( $ch , CURLOPT_CONNECTTIMEOUT , $timeout );
		// if you want to force to ipv6, uncomment the following line
		//curl_setopt( $ch , CURLOPT_IPRESOLVE , 'CURLOPT_IPRESOLVE_V6');
	    $tmp = curl_exec( $ch );
	    curl_close( $ch );
	    return $tmp;
	}*/

    /**
     *  HTTP GET request with curl that writes the curl result into a local file.
     *  @access  private
     *  @param   string   $url  String, containing the remote file URL to curl.
     *  @param   string   $local_file   String, containing the path to the file to save
     *                                  the curl result in to.
     *  @return  void
     */
    protected function get_file($url) {

       	curl_setopt($this->ch, CURLOPT_URL, $url);
		//curl_setopt($this->ch, CURLOPT_HEADER, false);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
	    //curl_setopt($this->ch, CURLOPT_USERAGENT, $this->CURL_UA);

	    // Save the curl buffer into a file
	    // - getting the default value of the download folder
	    //$this->path = self::download_folder;

	    /*if(is_dir($this->path) !== FALSE) {
            chmod($this->path, 0777); # Ensure permissions. Otherwise CURLOPT_FILE will fail!
            return TRUE;
        } else {
            return (bool) ! mkdir($this->path, 0777);
        }

        $file = fopen($this->path, 'w');
        curl_setopt($this->ch, CURLOPT_FILE, $file);*/
        //fclose($file);

        $this->curl = curl_exec ($this->ch);
        //var_dump("GET_FILE()");

        if (curl_errno($this->ch)) {
			throw new Exception("OMG! Something is wrong, aliens everywhere...", 1);
		}

     	//  HTTP HEAD requested with curl
     	//  - check return from curl for status code
        $curl_status = intval(curl_getinfo($this->ch, CURLINFO_HTTP_CODE));

        if ($curl_status !== 200) {
			throw new Exception("The ogres in YouTube servers are very busy now, please come back later.", 1);
	    }

        return $this->curl;
    }



	/*
	 * function to use cUrl to get the headers of the file
	 */
	/*function get_location($url) {
		global $config;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$r = curl_exec($ch);
		 foreach(explode("\n", $r) as $header) {
			if(strpos($header, 'Location: ') === 0) {
				return trim(substr($header,10));
			}
		 }
		return '';
	}*/

	public function get_size($url) {

		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt($this->ch, CURLOPT_HEADER, true);
		curl_setopt($this->ch, CURLOPT_NOBODY, true);
		curl_setopt($this->ch, CURLOPT_REFERER, $_SERVER['REMOTE_ADDR']);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($this->ch, CURLOPT_USERAGENT, $this->CURL_UA);

        //curl_setopt($this->ch, CURLOPT_POST, true);
        //curl_setopt($this->ch, CURLOPT_POSTFIELDS, $token);

		$this->curl = curl_exec($this->ch);

		if (curl_errno($this->ch)) {
			throw new Exception("OMG! Something is wrong, aliens... please try again.", 1);
			return false;
		}

		$curl_status = intval(curl_getinfo($this->ch, CURLINFO_HTTP_CODE));
		if ($curl_status !== 200) {
			throw new Exception("The ogres in YouTube servers are very busy now, please wait a minute and try again.", 1);
			return false;
	    }

	    foreach(explode("\n", $this->curl) as $header) {
			if(strpos($header, 'Content-Length:') === 0) {
				return trim(substr($header,16));
			}
		}

		return false;
	}

	/*function get_description($url) {
		$fullpage = curlGet($url);
		$dom = new DOMDocument();
		@$dom->loadHTML($fullpage);
		$xpath = new DOMXPath($dom);
		$tags = $xpath->query('//div[@class="info-description-body"]');
		foreach ($tags as $tag) {
			$my_description .= (trim($tag->nodeValue));
		}

		return utf8_decode($my_description);
	}*/
}
