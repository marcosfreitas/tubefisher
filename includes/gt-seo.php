<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<meta name="reply-to" content="marcosfreitas@c4network.com.br">
<meta name="robots" content="all">

<title>TubeFisher - Download videos from Youtube</title>

<!--<link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicons/apple-touch-icon-57x57.png?v2">
<link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicons/apple-touch-icon-114x114.png?v2">
<link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicons/apple-touch-icon-72x72.png?v2">
<link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicons/apple-touch-icon-144x144.png?v2">
<link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicons/apple-touch-icon-60x60.png?v2">
<link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicons/apple-touch-icon-120x120.png?v2">
<link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicons/apple-touch-icon-76x76.png?v2">
<link rel="icon" type="image/png" href="assets/images/favicons/favicon-96x96.png?v2" sizes="96x96">
<link rel="icon" type="image/png" href="assets/images/favicons/favicon-16x16.png?v2" sizes="16x16">
<link rel="icon" type="image/png" href="assets/images/favicons/favicon-32x32.png?v2" sizes="32x32">
<meta name="msapplication-TileColor" content="#1f93d1">
<meta name="msapplication-TileImage" content="assets/images/favicons/mstile-144x144.png?v2">

<meta name="description" content="">-->
<meta name="keywords" content="Video downloader, download youtube, video download, youtube video, youtube downloader, download youtube FLV, download youtube MP4, download youtube 3GP, php video downloader">

<!-- complementos para o SEO -->
<!-- se comportar como applicativo da homescreen -->
<meta name="mobile-web-app-capable" content="yes">

<!-- cor do tema do chrome no mobile -->
<meta name="theme-color" content="#212121">



<!--<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@imirante">
<meta name="twitter:title" content="Imirante.com">
<meta name="twitter:description" content="Siga o imirante.com e saiba tudo que acontece no Maranh�o e no Brasil. O melhor acervo de not�cias sobre: Maranh�o, esporte, entretenimento, servi�os e coberturas com v�deos e fotos.">
<meta name="twitter:image:src" content="http://assets.imirante.com/2.1/images/brands/fb-logo-imirante.png">

<meta property="og:title" content="Imirante.com">
<meta property="og:locale" content="pt_BR">
<meta property="og:type" content="website">
<meta property="og:url" content="http://imirante.com">
<meta property="og:image" content="http://assets.imirante.com/2.1/images/fb-logo-imirante.png">
<meta property="og:description" content="Siga o imirante.com e saiba tudo que acontece no Maranh�o e no Brasil. O melhor acervo de not�cias sobre: Maranh�o, esporte, entretenimento, servi�os e coberturas com v�deos e fotos.">
<meta property="og:site_name" content="Imirante.com">
<meta property="fb:app_id" content="">

<script data-schema="organization" type="application/ld+json">
	{ "@context" : "http://schema.org", "@type" : "Organization", "name" : "Imirante", "url" : "http://imirante.com/guia/2/sao-luis/", "logo" : "http://assets.imirante.com/3.0/images/brands/fb-logo-imirante.png", "sameAs" : ["https://www.facebook.com/portalimirante","https://twitter.com/imirante"]}
</script>

<script type="application/ld+json">
{
   "@context": "http://schema.org",
   "@type": "WebSite",
   "url": "http://www.imirante.com/",
   "potentialAction": {
     "@type": "SearchAction",
     "target": "http://imirante.com/plantaoi/?query={search_term_string}",
     "query-input": "required name=search_term_string"
   }
}
</script>
-->
