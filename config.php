<?php

	/* Sessions */

	//if (session_status() !== PHP_SESSION_ACTIVE && session_status() !== 2) {}
	/* Define o limitador de cache para 'private' */
	//session_cache_limiter('private');

	/* Define o limite de tempo do cache em 30 minutos */
	session_cache_expire(30);

	/* Inicia a sessão */
	session_name("gettube");
	session_start();

	/* Timezone */
	// Set your default timezone
	// use this link: http://php.net/manual/en/timezones.php

	// trying to determine user timezone by ip address.
	// if can't do this, America/New_York is the default
	// if hosted on localhost the request to api won't work.

	if (!isset($_SESSION['user_timezone'])) {

		$ip     = $_SERVER['REMOTE_ADDR']; // means we got user's IP address
		$json   = file_get_contents( 'http://ip-api.com/json/' . $ip); // this one service we gonna use to obtain timezone by IP
		// maybe it's good to add some checks (if/else you've got an answer and if json could be decoded, etc.)
		$ipData = json_decode( $json, true);

		if (isset($ipData['timezone'])) {
		    $tz = new DateTimeZone( $ipData['timezone']);
		    $_SESSION['user_timezone'] = $tz;
		    //$now = new DateTime( 'now', $tz); // DateTime object corellated to user's timezone
		    date_default_timezone_set($tz);
		} else {
		   // we can't determine a timezone - do something else...
			date_default_timezone_set("America/New_York");
		}

	}

	include_once('helpers/functions.php');
?>
